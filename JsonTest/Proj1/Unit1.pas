unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
    System.JSON.Serializers
  , System.JSON.Converters
  , System.Generics.Collections
  , System.IOUtils
  , Unit2
  ;

procedure TForm1.Button1Click(Sender: TObject);
var
  CoffeeShop : TCoffeeShop;
  serializer: TJsonSerializer;
  s : string;


begin
  CoffeeShop := TCoffeeShop.Create;
  try
    CoffeeShop.ShopName := 'ラビットハウス';
    CoffeeShop.Clerks.Add(TClerk.Create('ココア',17));
    CoffeeShop.Clerks.Add(TClerk.Create('チノ',15));
    CoffeeShop.Clerks.Add(TClerk.Create('リゼ',17));

    serializer := TJsonSerializer.Create;
    try

      s := serializer.Serialize(CoffeeShop);
      Memo1.Text := s;

      TFile.WriteAllText('CoffeeShop.Json',s);

    finally
      serializer.Free;
    end;

  finally
    CoffeeShop.Clerks.Clear();
    CoffeeShop.Free;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  CoffeeShop : TCoffeeShop;
  serializer: TJsonSerializer;
  s : string;
  Clerk : TClerk;
begin

  s := TFile.ReadAllText('CoffeeShop.Json',TEncoding.UTF8);

  serializer := TJsonSerializer.Create;
  try
    CoffeeShop := serializer.Deserialize<TCoffeeShop>(s);
    try
      Memo1.Clear;
      Memo1.Lines.Add(CoffeeShop.ShopName);
      for Clerk in CoffeeShop.Clerks do
      begin
        Memo1.Lines.Add(Clerk.Name + '(' + Clerk.Age.ToString() + ')');
      end;
    finally
      CoffeeShop.Clerks.Clear();
      CoffeeShop.Free;
    end;
  finally
    serializer.Free
  end;


end;

end.
