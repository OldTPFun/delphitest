﻿unit Unit2;

interface
uses
  //Json.SerializersとConverterを使用する。
    System.JSON.Serializers
  , System.JSON.Converters
  //TList<T>を使用する
  , System.Generics.Collections
  ;

type

  //パブリックメンバーをシリアライズ対象にする属性
  [JsonSerialize(TJsonMemberSerialization.&Public)]
  TClerk = class
    private
      FName: string;
      FAge: integer;

      procedure SetAge(const Value: integer);
      procedure SetName(const Value: string);
    public
      property Name : string read FName write SetName;
      property Age : integer read FAge write SetAge;
      constructor Create; overload;
      constructor Create(const vName : string; const vAge : integer); overload;
  end;

  //TClerk型のリスト用のコンバーター
  TJsonClerkListConverter = class(TJsonListConverter<TClerk>);

  [JsonSerialize(TJsonMemberSerialization.&Public)]
  TCoffeeShop = class
    private
    FClerks: TObjectList<TClerk>;
    FShopName: string;
    procedure SetClerks(const Value: TObjectList<TClerk>);
    procedure SetShopName(const Value: string);
    public
      property ShopName : string read FShopName write SetShopName;

      //TClerkクラスのジェネリックリスト用のコンバーターを登録
      [JsonConverter(TJsonClerkListConverter)]
      property Clerks : TObjectList<TClerk> read FClerks write SetClerks;

      public
        constructor Create;
        destructor Destroy; override;
  end;

  //注意：かきのコンバーターは失敗する。
  //TCoffieShopDicConverter = class(TJsonDictionaryConverter<string, TCoffeeShop>);

  //定義済みのキーが文字列のコンバータからTCoffeeShop型のインスタンスが値の
  //コンバータを作成
  //TJsonDictionaryConverter<K,V>からキーの型を決定したコンバータを派生
  //PropertyToKey,KeyToPropertyを実装後、
  //値の型に応じたコンバータを作成
  TCoffieShopDicConverter = class(TJsonStringDictionaryConverter<TCoffeeShop>);

  [JsonSerialize(TJsonMemberSerialization.&Public)]
  TCofeeShopList = class
  private
    FTownName: String;
    FShops: TObjectDictionary<string, TCoffeeShop>;
    procedure SetTownName(const Value: String);
    procedure SetShops(const Value: TObjectDictionary<string, TCoffeeShop>);
    public
      property TownName : String read FTownName write SetTownName;
      [JsonConverter(TCoffieShopDicConverter)]
      property Shops : TObjectDictionary<string, TCoffeeShop> read FShops write SetShops;
      constructor Create;
      destructor Destroy; override;
  end;

implementation

{ TClerk }

constructor TClerk.Create(const vName: string; const vAge: integer);
begin
   FName := vName;
   FAge := vAge;
end;

constructor TClerk.Create;
begin

end;

procedure TClerk.SetAge(const Value: integer);
begin
  FAge := Value;
end;

procedure TClerk.SetName(const Value: string);
begin
  FName := Value;
end;

{ TCoffeeShop }

constructor TCoffeeShop.Create;
begin
  FClerks := TObjectList<TClerk>.Create(true);
end;

destructor TCoffeeShop.Destroy;
begin
  if assigned(FClerks) then FClerks.Free;
  inherited;
end;

procedure TCoffeeShop.SetClerks(const Value: TObjectList<TClerk>);
begin
  FClerks := Value;
end;

procedure TCoffeeShop.SetShopName(const Value: string);
begin
  FShopName := Value;
end;

{ TCofeeShopList }

constructor TCofeeShopList.Create;
var
  Ownership :TDictionaryOwnerships;
begin
  Ownership := [doOwnsValues];
  FShops := TObjectDictionary<string, TCoffeeShop>.Create(Ownership);
end;

destructor TCofeeShopList.Destroy;
var
  key : string;
  //CoffeeShop : TCoffeeShop;
begin
  if assigned(FShops) then
  begin
    {for key in FShops.Keys do
    begin
      CoffeeShop := FShops[key];
      FShops[key] := nil;
      if assigned(CoffeeShop) then CoffeeShop.Free;
    end;}
    FShops.Free;
  end;

  inherited;
end;

procedure TCofeeShopList.SetShops(
  const Value: TObjectDictionary<string, TCoffeeShop>);
begin
  FShops := Value;
end;

procedure TCofeeShopList.SetTownName(const Value: String);
begin
  FTownName := Value;
end;

end.
