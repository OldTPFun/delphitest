unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private �錾 }
  public
    { Public �錾 }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
    System.JSON.Serializers
  , System.JSON.Converters
  , System.Generics.Collections
  , System.IOUtils
  , Unit2
  ;

procedure TForm1.Button1Click(Sender: TObject);
var
  CoffeeShop1,CoffeeShop2 : TCoffeeShop;
  serializer: TJsonSerializer;
  CofeeShopList : TCofeeShopList;
  s : string;
begin
  CofeeShopList := TCofeeShopList.Create;
  CoffeeShop1 := TCoffeeShop.Create;
  CoffeeShop2 := TCoffeeShop.Create;
  try
    CoffeeShop1.ShopName := '���r�b�g�n�E�X';
    CoffeeShop1.Clerks.Add(TClerk.Create('�R�R�A',17));
    CoffeeShop1.Clerks.Add(TClerk.Create('�`�m',15));
    CoffeeShop1.Clerks.Add(TClerk.Create('���[',17));

    CoffeeShop2.ShopName := '�Óe��';
    CoffeeShop2.Clerks.Add(TClerk.Create('�`��',17));

    CofeeShopList.TownName := '�ؑg�݂̉ƂƐΏ�̊X';
    CofeeShopList.Shops.Add(CoffeeShop1.ShopName,CoffeeShop1);
    CofeeShopList.Shops.Add(CoffeeShop2.ShopName,CoffeeShop2);

    serializer := TJsonSerializer.Create;
    try

      s := serializer.Serialize(CofeeShopList);
      Memo1.Text := s;

      TFile.WriteAllText('CoffeeShop.Json',s);

    finally
      serializer.Free;
    end;

  finally
    CofeeShopList.Shops.Clear;
    CofeeShopList.Free;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  CoffeeShop : TCoffeeShop;
  CofeeShopList : TCofeeShopList;
  serializer: TJsonSerializer;
  s : string;
  Clerk : TClerk;
begin

  s := TFile.ReadAllText('CoffeeShop.Json',TEncoding.UTF8);

  serializer := TJsonSerializer.Create;
  try
    CofeeShopList := serializer.Deserialize<TCofeeShopList>(s);
    try
      Memo1.Clear;
      Memo1.Lines.Add(CofeeShopList.TownName);
      for CoffeeShop in CofeeShopList.Shops.Values do
      begin
        Memo1.Lines.Add(CoffeeShop.ShopName);
        for Clerk in CoffeeShop.Clerks do
          begin
          Memo1.Lines.Add(Clerk.Name + '(' + Clerk.Age.ToString() + ')');
        end;
      end;

    finally
      //CoffeeShop.Clerks.Clear();
      CofeeShopList.Free;
    end;
  finally
    serializer.Free
  end;


end;

end.
